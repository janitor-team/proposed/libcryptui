# Italian translation of libcryptui
# Copyright (C) 2006, 2007, 2008, 2009, 2010 the Free Software Foundation, Inc.
# This file is distributed under the same license as the libcryptui package.
# Luigi maselli <metnik at tiscali.it>, 2003
#
# keyserver -> server di chiavi come nel Manuale GNU sulla privacy
# passphrase -> invariato come nel Manuale GNU sulla privacy
# password keyring -> lascio solo portachiavi anche per uniformarsi col vecchio portachiavi di GNOME
# Milo Casagrande <milo@ubuntu.com>, 2006, 2007, 2008, 2009, 2010, 2011, 2013.
#
msgid ""
msgstr ""
"Project-Id-Version: libcryptui\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-08-26 09:53+0200\n"
"PO-Revision-Date: 2013-08-26 09:52+0200\n"
"Last-Translator: Milo Casagrande <milo@ubuntu.com>\n"
"Language-Team: Italian <tp@lists.linux.it>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8-bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Gtranslator 2.91.6\n"

#: ../daemon/seahorse-daemon.c:53
msgid "Do not run seahorse-daemon as a daemon"
msgstr "Non eseguire seahorse-daemon come un demone"

#: ../daemon/seahorse-daemon.c:83
msgid "couldn't fork process"
msgstr "impossibile eseguire un fork del processo"

#: ../daemon/seahorse-daemon.c:89
msgid "couldn't create new process group"
msgstr "impossibile creare il gruppo del nuovo processo"

#: ../daemon/seahorse-daemon.c:237
msgid "Encryption Daemon (Seahorse)"
msgstr "Demone di cifratura (Seahorse)"

#: ../daemon/seahorse-notification.c:576 ../daemon/seahorse-notification.c:604
msgid "Key Imported"
msgid_plural "Keys Imported"
msgstr[0] "Chiave importata"
msgstr[1] "Chiavi importate"

#: ../daemon/seahorse-notification.c:580 ../daemon/seahorse-notification.c:603
#, c-format
msgid "Imported %i key"
msgid_plural "Imported %i keys"
msgstr[0] "%i chiave importata"
msgstr[1] "%i chiavi importate"

#: ../daemon/seahorse-notification.c:582
#, c-format
msgid "Imported a key for"
msgid_plural "Imported keys for"
msgstr[0] "Importata una chiave per"
msgstr[1] "Importate chiavi per"

#: ../daemon/seahorse-gpgme.c:72
msgid "Decryption failed. You probably do not have the decryption key."
msgstr ""
"Decifrazione non riuscita. Probabilmente non è presente la chiave di "
"decifrazione."

#: ../daemon/seahorse-gpgme-generate.c:62
msgid "RSA"
msgstr "RSA"

#: ../daemon/seahorse-gpgme-generate.c:63
msgid "DSA Elgamal"
msgstr "DSA ElGamal"

#: ../daemon/seahorse-gpgme-generate.c:64
msgid "DSA (sign only)"
msgstr "DSA (solo firma)"

#: ../daemon/seahorse-gpgme-generate.c:65
msgid "RSA (sign only)"
msgstr "RSA (solo firma)"

#: ../daemon/seahorse-gpgme-generate.c:81
msgid "Couldn't generate PGP key"
msgstr "Impossibile generare la chiave PGP"

#: ../daemon/seahorse-gpgme-generate.c:141
msgid "Passphrase for New PGP Key"
msgstr "Passphrase per la nuova chiave PGP"

#: ../daemon/seahorse-gpgme-generate.c:142
msgid "Enter the passphrase for your new key twice."
msgstr "Inserire due volte la passphrase per la nuova chiave."

#: ../daemon/seahorse-gpgme-generate.c:151
msgid "Couldn't generate key"
msgstr "Impossibile generare la chiave"

#: ../daemon/seahorse-gpgme-generate.c:153
msgid "Generating key"
msgstr "Generazione della chiave"

#: ../daemon/seahorse-gpgme-source.c:98
#, c-format
msgid "Wrong passphrase."
msgstr "Passphrase non corretta."

#: ../daemon/seahorse-gpgme-source.c:102
#, c-format
msgid "Enter new passphrase for '%s'"
msgstr "Inserire una nuova passphrase per «%s»"

#: ../daemon/seahorse-gpgme-source.c:104
#, c-format
msgid "Enter passphrase for '%s'"
msgstr "Inserire passphrase per «%s»"

#: ../daemon/seahorse-gpgme-source.c:107
msgid "Enter new passphrase"
msgstr "Inserire una nuova passphrase"

#: ../daemon/seahorse-gpgme-source.c:109
msgid "Enter passphrase"
msgstr "Inserire passphrase"

#. TODO: We can use the GPGME progress to make this more accurate
#: ../daemon/seahorse-gpgme-source.c:712
#, c-format
msgid "Loaded %d key"
msgid_plural "Loaded %d keys"
msgstr[0] "Caricata %d chiave"
msgstr[1] "Caricate %d chiavi"

#: ../daemon/seahorse-gpgme-source.c:765
msgid "Loading Keys..."
msgstr "Caricamento chiavi..."

#: ../daemon/seahorse-gpgme-source.c:808
msgid ""
"Invalid key data (missing UIDs). This may be due to a computer with a date "
"set in the future or a missing self-signature."
msgstr ""
"Dati delle chiavi non validi (manca UID). Può essere causato da un computer "
"con una data impostata male (nel futuro) o dalla mancanza di una auto-firma."

#: ../daemon/seahorse-gpgme-source.c:898
msgid "Importing Keys"
msgstr "Importazione chiavi"

#: ../daemon/seahorse-gpgme-source.c:931
msgid "Exporting Keys"
msgstr "Esportazione chiavi"

# (NdT) femminile, è per la fiducia?
#: ../daemon/seahorse-gpgme-subkey.c:197 ../daemon/seahorse-validity.c:40
msgid "Unknown"
msgstr "Sconosciuta"

#: ../daemon/seahorse-gpgme-subkey.c:199
msgid "ElGamal"
msgstr "ElGamal"

#: ../daemon/seahorse-notify.xml.h:1
msgid "Notification Messages"
msgstr "Messaggi di notifica"

#: ../daemon/seahorse-object.c:317
msgid "Symmetric Key"
msgstr "Chiave simmetrica"

#: ../daemon/seahorse-object.c:320
msgid "Public Key"
msgstr "Chiave pubblica"

#: ../daemon/seahorse-object.c:323
msgid "Private Key"
msgstr "Chiave privata"

#: ../daemon/seahorse-object.c:326
msgid "Credentials"
msgstr "Credenziali"

#.
#. * Translators: "This object is a means of storing items such as
#. * name, email address, etc. that make up one's digital identity.
#.
#: ../daemon/seahorse-object.c:333
msgid "Identity"
msgstr "Identità"

#: ../daemon/seahorse-passphrase.c:196
msgid "Passphrase"
msgstr "Passphrase"

#: ../daemon/seahorse-passphrase.c:199
msgid "Password:"
msgstr "Password:"

#. The second and main entry
#: ../daemon/seahorse-passphrase.c:263
msgid "Confirm:"
msgstr "Conferma:"

#: ../daemon/seahorse-pgp-generate.xml.h:1
msgid "Algorithms here"
msgstr "Algoritmo"

#: ../daemon/seahorse-pgp-generate.xml.h:2
msgid "New PGP Key"
msgstr "Nuova chiave PGP"

#: ../daemon/seahorse-pgp-generate.xml.h:3
msgid "A PGP key allows you to encrypt email or files to other people."
msgstr "Una chiave PGP consente di cifrare email o file ad altre persone."

#: ../daemon/seahorse-pgp-generate.xml.h:4
msgid "Full _Name:"
msgstr "_Nome e cognome:"

#: ../daemon/seahorse-pgp-generate.xml.h:5
msgid "_Email Address:"
msgstr "Indirizzo _email:"

#: ../daemon/seahorse-pgp-generate.xml.h:6
msgid "_Comment:"
msgstr "_Commento:"

#: ../daemon/seahorse-pgp-generate.xml.h:7
msgid "Ne_ver Expires"
msgstr "Ness_una scadenza"

#: ../daemon/seahorse-pgp-generate.xml.h:8
msgid "Encryption _Type:"
msgstr "_Tipo di cifratura:"

#: ../daemon/seahorse-pgp-generate.xml.h:9
msgid "Key _Strength (bits):"
msgstr "_Dimensione della chiave (in bit):"

#: ../daemon/seahorse-pgp-generate.xml.h:10
msgid "E_xpiration Date:"
msgstr "Data di scaden_za:"

#: ../daemon/seahorse-pgp-generate.xml.h:11
msgid "<b>_Advanced key options</b>"
msgstr "<b>Opzioni _avanzate</b>"

#: ../daemon/seahorse-pgp-generate.xml.h:12
msgid "Generate a new key"
msgstr "Genera una nuova chiave"

#: ../daemon/seahorse-pgp-generate.xml.h:13
msgid "C_reate"
msgstr "C_rea"

#: ../daemon/seahorse-pgp-key.c:179
msgid "Private PGP Key"
msgstr "Chiave PGP privata"

#: ../daemon/seahorse-pgp-key.c:182
msgid "Public PGP Key"
msgstr "Chiave PGP pubblica"

#: ../daemon/seahorse-pgp-key.c:527
msgid "Expired"
msgstr "Scaduto"

#: ../daemon/seahorse-pgp-subkey.c:366
msgid "Key"
msgstr "Chiave"

#: ../daemon/seahorse-pgp-subkey.c:371
#, c-format
msgid "Subkey %d of %s"
msgstr "Sottochiave %d di %s"

#: ../daemon/seahorse-progress.xml.h:1
msgid "Progress Title"
msgstr "Titolo dell'avanzamento"

#: ../daemon/seahorse-service.c:161 ../daemon/seahorse-service.c:267
#, c-format
msgid "Invalid or unrecognized key type: %s"
msgstr "Tipo di chiave non valido o non riconosciuto: %s"

#: ../daemon/seahorse-service.c:229
#, c-format
msgid "This keytype is not supported: %s"
msgstr "Questo tipo di chiave non è supportato: %s"

#: ../daemon/seahorse-service.c:331 ../daemon/seahorse-service-keyset.c:180
#: ../daemon/seahorse-service-keyset.c:223
#, c-format
msgid "Invalid or unrecognized key: %s"
msgstr "Chiave non valida o non riconosciuta: %s"

#. TRANSLATORS: <key id='xxx'> is a custom markup tag, do not translate.
#: ../daemon/seahorse-service-crypto.c:204
#, c-format
msgid "Signed by <i><key id='%s'/> <b>expired</b></i> on %s."
msgstr "Firmato da <i><key id='%s'/>, <b>scaduta</b></i> il %s."

#: ../daemon/seahorse-service-crypto.c:205
msgid "Invalid Signature"
msgstr "Firma non valida"

#: ../daemon/seahorse-service-crypto.c:211
#, c-format
msgid "Signed by <i><key id='%s'/></i> on %s <b>Expired</b>."
msgstr "Firmato da <i><key id='%s'/></i> il %s, <b>scaduta</b>."

#: ../daemon/seahorse-service-crypto.c:212
msgid "Expired Signature"
msgstr "Firma scaduta"

#: ../daemon/seahorse-service-crypto.c:218
#, c-format
msgid "Signed by <i><key id='%s'/> <b>Revoked</b></i> on %s."
msgstr "Firmato da <i><key id='%s'/>, <b>revocata</b></i> il %s."

#: ../daemon/seahorse-service-crypto.c:219
msgid "Revoked Signature"
msgstr "Firma revocata"

#. TRANSLATORS: <key id='xxx'> is a custom markup tag, do not translate.
#: ../daemon/seahorse-service-crypto.c:225
#, c-format
msgid "Signed by <i><key id='%s'/></i> on %s."
msgstr "Firmato da <i><key id='%s'/></i> il %s."

#: ../daemon/seahorse-service-crypto.c:226
msgid "Good Signature"
msgstr "Firma valida"

#: ../daemon/seahorse-service-crypto.c:231
msgid "Signing key not in keyring."
msgstr "Chiave di firma non presente nel portachiavi."

#: ../daemon/seahorse-service-crypto.c:232
msgid "Unknown Signature"
msgstr "Firma sconosciuta"

#: ../daemon/seahorse-service-crypto.c:236
msgid "Bad or forged signature. The signed data was modified."
msgstr "Firma errata o contraffatta. I dati firmati sono stati modificati."

#: ../daemon/seahorse-service-crypto.c:237
msgid "Bad Signature"
msgstr "Firma errata"

#: ../daemon/seahorse-service-crypto.c:245
msgid "Couldn't verify signature."
msgstr "Impossibile verificare la firma."

#: ../daemon/seahorse-service-crypto.c:317
#, c-format
msgid "Recipients specified for symmetric encryption"
msgstr "Destinatari specificati per cifratura simmetrica"

#: ../daemon/seahorse-service-crypto.c:326
#: ../daemon/seahorse-service-crypto.c:673
#, c-format
msgid "Invalid or unrecognized signer: %s"
msgstr "Firmatario non valido o non riconosciuto: %s"

#: ../daemon/seahorse-service-crypto.c:333
#: ../daemon/seahorse-service-crypto.c:680
#, c-format
msgid "Key is not valid for signing: %s"
msgstr "La chiave non è valida per firmare: %s"

#: ../daemon/seahorse-service-crypto.c:346
#, c-format
msgid "Invalid or unrecognized recipient: %s"
msgstr "Destinatario non valido o non riconosciuto: %s"

#: ../daemon/seahorse-service-crypto.c:354
#, c-format
msgid "Key is not a valid recipient for encryption: %s"
msgstr "La chiave non è un destinatario valido per la cifratura: %s"

#: ../daemon/seahorse-service-crypto.c:363
#, c-format
msgid "No recipients specified"
msgstr "Nessun destinatario specificato"

#: ../daemon/seahorse-service-crypto.c:460
#, c-format
msgid "Invalid key type for decryption: %s"
msgstr "Chiave non valida per decifrare: %s"

#: ../daemon/seahorse-service-crypto.c:575
#: ../daemon/seahorse-service-crypto.c:773
#, c-format
msgid "Please set clearuri"
msgstr "Impostare clearuri"

#: ../daemon/seahorse-service-crypto.c:581
#: ../daemon/seahorse-service-crypto.c:779
#, c-format
msgid "Please set crypturi"
msgstr "Impostare crypturi"

#: ../daemon/seahorse-service-crypto.c:591
#, c-format
msgid "Error opening clearuri"
msgstr "Errore nell'aprire clearuri"

#: ../daemon/seahorse-service-crypto.c:668
#, c-format
msgid "No signer specified"
msgstr "Nessun firmatario specificato"

#: ../daemon/seahorse-service-crypto.c:857
#, c-format
msgid "Invalid key type for verifying: %s"
msgstr "Chiave non valida per verificare: %s"

#: ../daemon/seahorse-service-keyset.c:273
#, c-format
msgid "Invalid key id: %s"
msgstr "Id della chiave non valido: %s"

#: ../daemon/seahorse-unknown.c:58
msgid "Unavailable"
msgstr "Non disponibile"

#: ../daemon/seahorse-util.c:231
msgid "%Y-%m-%d"
msgstr "%d/%m/%Y"

#: ../daemon/seahorse-util.c:510
msgid "Key Data"
msgstr "Dati della chiave"

#: ../daemon/seahorse-util.c:512
msgid "Multiple Keys"
msgstr "Chiavi multiple"

#: ../daemon/seahorse-util.c:768
msgid "Couldn't run file-roller"
msgstr "Impossibile avviare file-roller"

#: ../daemon/seahorse-util.c:774
msgid "Couldn't package files"
msgstr "Impossibile archiviare i file"

#: ../daemon/seahorse-util.c:775
msgid "The file-roller process did not complete successfully"
msgstr "Il processo di file-roller non è terminato con successo"

#. Filter for PGP keys. We also include *.asc, as in many
#. cases that extension is associated with text/plain
#: ../daemon/seahorse-util.c:944
msgid "All key files"
msgstr "Tutti i file chiave"

#: ../daemon/seahorse-util.c:951 ../daemon/seahorse-util.c:999
msgid "All files"
msgstr "Tutti i file"

#: ../daemon/seahorse-util.c:992
msgid "Archive files"
msgstr "File archivio"

#: ../daemon/seahorse-util.c:1062
msgid ""
"<b>A file already exists with this name.</b>\n"
"\n"
"Do you want to replace it with a new file?"
msgstr ""
"<b>Esiste già un file con questo nome.</b>\n"
"\n"
"Sostituirlo con il nuovo file?"

#: ../daemon/seahorse-util.c:1065
msgid "_Replace"
msgstr "_Sostituisci"

#: ../daemon/seahorse-validity.c:42
msgctxt "Validity"
msgid "Never"
msgstr "Nessuna"

#: ../daemon/seahorse-validity.c:44
msgid "Marginal"
msgstr "Marginale"

#: ../daemon/seahorse-validity.c:46
msgid "Full"
msgstr "Piena"

#: ../daemon/seahorse-validity.c:48
msgid "Ultimate"
msgstr "Completa"

#: ../daemon/seahorse-validity.c:50
msgid "Disabled"
msgstr "Disabilitata"

#: ../daemon/seahorse-validity.c:52
msgid "Revoked"
msgstr "Revocata"

#: ../daemon/seahorse-widget.c:433
#, c-format
msgid "Could not display help: %s"
msgstr "Impossibile visualizzare l'aiuto: %s"

#: ../libcryptui/cryptui.c:307
msgid ""
"No encryption keys were found with which to perform the operation you "
"requested.  The program <b>Passwords and Encryption Keys</b> will now be "
"started so that you may either create a key or import one."
msgstr ""
"Non è stata trovata alcuna chiave di cifratura con cui poter eseguire "
"l'operazione richiesta. Il programma <b>Password e chiavi di cifratura</b> "
"verrà avviato in modo tale da poter creare o importare una chiave."

#: ../libcryptui/cryptui.c:333
msgid ""
"No encryption keys were found. In order to perform public key encryption, "
"the <b>Passwords and Encryption Keys</b> program can be started to create or "
"import a public key. It is also possible to use a shared passphrase instead."
msgstr ""
"Non è stata trovata alcuna chiave di cifratura. Per poter eseguire cifrature "
"a chiave pubblica, il programma <b>Password e chiavi di cifratura</b> può "
"essere avviato per creare o importare una chiave pubblica. È inoltre anche "
"possibile utilizzare una passphrase condivisa."

#: ../libcryptui/cryptui.c:336
msgid "Use a shared passphrase"
msgstr "Usare una passphrase condivisa"

#: ../libcryptui/cryptui.c:337
msgid "Create or import a key"
msgstr "Crea o importa una chiave"

#: ../libcryptui/cryptui-key-chooser.c:177
msgid "All Keys"
msgstr "Tutte le chiavi"

#: ../libcryptui/cryptui-key-chooser.c:178
msgid "Selected Recipients"
msgstr "Destinatari selezionati"

#: ../libcryptui/cryptui-key-chooser.c:179
msgid "Search Results"
msgstr "Risultati ricerca"

#. Filter Label
#: ../libcryptui/cryptui-key-chooser.c:189
msgid "Search _for:"
msgstr "C_erca:"

#. first of the group
#: ../libcryptui/cryptui-key-chooser.c:235
msgid "Use passphrase only"
msgstr "Usare solo passphrase"

#: ../libcryptui/cryptui-key-chooser.c:244
msgid "Choose a set of recipients:"
msgstr "Scegliere i destinatari:"

#: ../libcryptui/cryptui-key-chooser.c:290
msgid "None (Don't Sign)"
msgstr "Nessuno (non firmare)"

#: ../libcryptui/cryptui-key-chooser.c:303
#, c-format
msgid "Sign this message as %s"
msgstr "Firmare questo messaggio come %s"

#. Sign Label
#: ../libcryptui/cryptui-key-chooser.c:330
msgid "_Sign message as:"
msgstr "_Firma messaggio come:"

#. TODO: Icons
#. The name column
#: ../libcryptui/cryptui-key-list.c:140
msgid "Name"
msgstr "Nome"

#. The keyid column
#: ../libcryptui/cryptui-key-list.c:145
msgid "Key ID"
msgstr "ID chiave"

#: ../libegg/egg-datetime.c:317
msgid "Display flags"
msgstr "Visualizza flag"

#: ../libegg/egg-datetime.c:318
msgid "Displayed date and/or time properties"
msgstr "Proprietà data od orario visualizzate"

# (ndt) pigro era troppo banale...
#: ../libegg/egg-datetime.c:323
msgid "Lazy mode"
msgstr "Modalità apatica"

#: ../libegg/egg-datetime.c:324
msgid "Lazy mode doesn't normalize entered date and time values"
msgstr "La modalità apatica non normalizza i valori di data e orario inseriti"

#: ../libegg/egg-datetime.c:329
msgid "Year"
msgstr "Anno"

#: ../libegg/egg-datetime.c:330
msgid "Displayed year"
msgstr "Anno visualizzato"

#: ../libegg/egg-datetime.c:335
msgid "Month"
msgstr "Mese"

#: ../libegg/egg-datetime.c:336
msgid "Displayed month"
msgstr "Mese visualizzato"

#: ../libegg/egg-datetime.c:341
msgid "Day"
msgstr "Giorno"

#: ../libegg/egg-datetime.c:342
msgid "Displayed day of month"
msgstr "Giorno del mese visualizzato"

#: ../libegg/egg-datetime.c:347
msgid "Hour"
msgstr "Ora"

#: ../libegg/egg-datetime.c:348
msgid "Displayed hour"
msgstr "Ora visualizzata"

#: ../libegg/egg-datetime.c:353
msgid "Minute"
msgstr "Minuto"

#: ../libegg/egg-datetime.c:354
msgid "Displayed minute"
msgstr "Minuto visualizzato"

# (ndT) metto al plurale, mi suona meglio
#: ../libegg/egg-datetime.c:359
msgid "Second"
msgstr "Secondi"

#: ../libegg/egg-datetime.c:360
msgid "Displayed second"
msgstr "Secondi visualizzati"

#: ../libegg/egg-datetime.c:365
msgid "Lower limit year"
msgstr "Limite inferiore dell'anno"

#: ../libegg/egg-datetime.c:366
msgid "Year part of the lower date limit"
msgstr "Parte relativa all'anno del limite inferiore della data"

#: ../libegg/egg-datetime.c:371
msgid "Upper limit year"
msgstr "Limite superiore dell'anno"

#: ../libegg/egg-datetime.c:372
msgid "Year part of the upper date limit"
msgstr "Parte relativa all'anno del limite superiore della data"

#: ../libegg/egg-datetime.c:377
msgid "Lower limit month"
msgstr "Limite inferiore del mese"

#: ../libegg/egg-datetime.c:378
msgid "Month part of the lower date limit"
msgstr "Parte relativa al mese del limite inferiore della data"

#: ../libegg/egg-datetime.c:383
msgid "Upper limit month"
msgstr "Limite superiore del mese"

#: ../libegg/egg-datetime.c:384
msgid "Month part of the upper date limit"
msgstr "Parte relativa al mese del limite superiore della data"

#: ../libegg/egg-datetime.c:389
msgid "Lower limit day"
msgstr "Limite inferiore del giorno"

#: ../libegg/egg-datetime.c:390
msgid "Day of month part of the lower date limit"
msgstr "Parte relativa al giorno del mese del limite inferiore della data"

#: ../libegg/egg-datetime.c:395
msgid "Upper limit day"
msgstr "Limite superiore del giorno"

#: ../libegg/egg-datetime.c:396
msgid "Day of month part of the upper date limit"
msgstr "Parte relativa al giorno del mese del limite superiore della data"

#: ../libegg/egg-datetime.c:401
msgid "Lower limit hour"
msgstr "Limite inferiore dell'ora"

#: ../libegg/egg-datetime.c:402
msgid "Hour part of the lower time limit"
msgstr "Parte relativa all'ora del limite inferiore dell'orario"

#: ../libegg/egg-datetime.c:407
msgid "Upper limit hour"
msgstr "Limite superiore dell'ora"

#: ../libegg/egg-datetime.c:408
msgid "Hour part of the upper time limit"
msgstr "Parte relativa all'ora del limite superiore dell'orario"

#: ../libegg/egg-datetime.c:413
msgid "Lower limit minute"
msgstr "Limite inferiore del minuto"

#: ../libegg/egg-datetime.c:414
msgid "Minute part of the lower time limit"
msgstr "Parte relativa al minuto del limite inferiore dell'orario"

#: ../libegg/egg-datetime.c:419
msgid "Upper limit minute"
msgstr "Limite superiore del minuto"

#: ../libegg/egg-datetime.c:420
msgid "Minute part of the upper time limit"
msgstr "Parte relativa al minuto del limite superiore dell'orario"

#: ../libegg/egg-datetime.c:425
msgid "Lower limit second"
msgstr "Limite inferiore del secondo"

#: ../libegg/egg-datetime.c:426
msgid "Second part of the lower time limit"
msgstr "Parte relativa ai secondi del limite inferiore dell'orario"

#: ../libegg/egg-datetime.c:431
msgid "Upper limit second"
msgstr "Limite superiore dei secondi"

#: ../libegg/egg-datetime.c:432
msgid "Second part of the upper time limit"
msgstr "Parte relativa ai secondi del limite superiore dell'orario"

#. Translate to calendar:week_start:1 if you want Monday to be the
#. * first day of the week; otherwise translate to calendar:week_start:0.
#. * Do *not* translate it to anything else, if it isn't calendar:week_start:1
#. * or calendar:week_start:0 it will not work.
#.
#: ../libegg/egg-datetime.c:474
msgid "calendar:week_start:0"
msgstr "calendar:week_start:1"

#: ../libegg/egg-datetime.c:496
msgid "Date"
msgstr "Data"

# (ndt) ATK
#: ../libegg/egg-datetime.c:496
msgid "Enter the date directly"
msgstr "Inserire direttamente la data"

# (ndt) pulsante
#
# ATK anche questi
#: ../libegg/egg-datetime.c:503
msgid "Select Date"
msgstr "Seleziona data"

# (ndt) suggerimento
#: ../libegg/egg-datetime.c:503
msgid "Select the date from a calendar"
msgstr "Seleziona la data da un calendario"

#: ../libegg/egg-datetime.c:521 ../libegg/egg-datetime.c:2196
msgid "Time"
msgstr "Orario"

#: ../libegg/egg-datetime.c:521
msgid "Enter the time directly"
msgstr "Inserire direttamente l'orario"

#: ../libegg/egg-datetime.c:528
msgid "Select Time"
msgstr "Seleziona orario"

#: ../libegg/egg-datetime.c:528
msgid "Select the time from a list"
msgstr "Seleziona l'orario da un elenco"

# (ndt) spero possa bastare così...
#. Translators: set this to anything else if you want to use a
#. * 24 hour clock.
#.
#: ../libegg/egg-datetime.c:793
msgid "24hr: no"
msgstr "si"

#: ../libegg/egg-datetime.c:797 ../libegg/egg-datetime.c:1257
#: ../libegg/egg-datetime.c:1261
msgid "AM"
msgstr "am"

#: ../libegg/egg-datetime.c:799 ../libegg/egg-datetime.c:1258
#: ../libegg/egg-datetime.c:1265
msgid "PM"
msgstr "pm"

#. Translators: This is hh:mm:ss AM/PM.
#: ../libegg/egg-datetime.c:807
#, c-format
msgid "%02d:%02d:%02d %s"
msgstr "%02d.%02d.%02d %s"

#. Translators: This is hh:mm AM/PM.
#: ../libegg/egg-datetime.c:810
#, c-format
msgid "%02d:%02d %s"
msgstr "%02d.%02d %s"

#. Translators: This is hh:mm:ss.
#: ../libegg/egg-datetime.c:814
#, c-format
msgid "%02d:%02d:%02d"
msgstr "%02d.%02d.%02d"

#. Translators: This is hh:mm.
#: ../libegg/egg-datetime.c:817
#, c-format
msgid "%02d:%02d"
msgstr "%02d.%02d"

# (ndt) dovrebbe funzionare...
#. TODO: should handle other display modes as well...
#. Translators: This is YYYY-MM-DD
#: ../libegg/egg-datetime.c:1173
#, c-format
msgid "%04d-%02d-%02d"
msgstr "%3$02d/%2$02d/%1$04d"

#. Translators: This is hh:mm:ss.
#: ../libegg/egg-datetime.c:1238
#, c-format
msgid "%u:%u:%u"
msgstr "%u.%u.%u"

#: ../libegg/eggdesktopfile.c:165
#, c-format
msgid "File is not a valid .desktop file"
msgstr "Il file non è un file .desktop valido"

#: ../libegg/eggdesktopfile.c:188
#, c-format
msgid "Unrecognized desktop file Version '%s'"
msgstr "Versione «%s» del file desktop non riconosciuta"

#: ../libegg/eggdesktopfile.c:968
#, c-format
msgid "Starting %s"
msgstr "Avvio di «%s»"

#: ../libegg/eggdesktopfile.c:1110
#, c-format
msgid "Application does not accept documents on command line"
msgstr "L'applicazione non accetta documenti sulla riga di comando"

#: ../libegg/eggdesktopfile.c:1178
#, c-format
msgid "Unrecognized launch option: %d"
msgstr "Opzione di lancio non riconosciuta: %d"

#: ../libegg/eggdesktopfile.c:1383
#, c-format
msgid "Can't pass document URIs to a 'Type=Link' desktop entry"
msgstr ""
"Impossibile passare l'URI di un documento a una voce desktop «Type=Link»"

#: ../libegg/eggdesktopfile.c:1404
#, c-format
msgid "Not a launchable item"
msgstr "Non è un oggetto lanciabile"

#: ../libegg/eggsmclient.c:225
msgid "Disable connection to session manager"
msgstr "Disabilita la connessione al gestore di sessione"

#: ../libegg/eggsmclient.c:228
msgid "Specify file containing saved configuration"
msgstr "Specifica il file contenente la configurazione salvata"

#: ../libegg/eggsmclient.c:228
msgid "FILE"
msgstr "FILE"

#: ../libegg/eggsmclient.c:231
msgid "Specify session management ID"
msgstr "Specifica l'ID di gestione sessione"

#: ../libegg/eggsmclient.c:231
msgid "ID"
msgstr "ID"

#: ../libegg/eggsmclient.c:252
msgid "Session management options:"
msgstr "Opzioni di gestione sessione:"

#: ../libegg/eggsmclient.c:253
msgid "Show session management options"
msgstr "Mostra le opzioni di gestione sessione"
